import styled from 'styled-components'

export const SubmitButton = styled.button`
    display: block;
    border-radius: 10px;
    padding: 5px;
    color: #fff;
    margin: auto;
    background: #232632;
`

export const Players = styled.div`
    margin-bottom: 30px;
`

export const Container = styled.div`
    background: #2b2e39;
    text-align: center;
    width: 80%;
    max-width: 600px;
    padding: 14px;
    border-radius: 14px;
    margin: auto;
`

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    color: #777;
    font-size: 1.8em
    margin: 0.5em 0;
    position: relative;
`

export const Box = styled.div`
    float: left;
    position: relative;
`

export const Row = styled.div`
    display: flex;
`