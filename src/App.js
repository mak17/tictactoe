import React from 'react';
import './App.css';
import Users from './components/Users.js';
import { Container } from './Theme/Theme.js'

function App() {
  return (
    <Container><Users /></Container> 
  );
}

export default App;
