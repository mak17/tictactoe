import React, { Component } from 'react';
import { Container, Box, Row } from '../Theme/Theme';

class Popup extends Component {  

    action = (box) => {
        if(box.value.length === 0 ) {
            this.props.click(box.id)
        }
    }

    render() {  
        return (  
            <Container>
                 {this.props.border.map(row => 
                    <Row>{
                        row.map(box => 
                        <Box id={box.id} 
                            onClick={ () => this.action(box) }
                            key={box.id}>
                            <p className="centerText">{box.value}</p>
                        </Box>)}
                    </Row>)}
            </Container>   
        );  
    }  
}  

export default Popup;