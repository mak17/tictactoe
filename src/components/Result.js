import React, { Component } from 'react';
import { Label, Players } from '../Theme/Theme.js'

class Users extends Component {
    state = {
        playerOne: '',
        playerTwo: '',
        pointsOne: 0,
        pointsTwo: 0,
        whichPlayer: 0
    }

    componentDidMount = () => {
        this.setState({
            playerOne: "Player 1",
            playerTwo: "Player 2",
            pointsOne: this.props.pointsOne,
            pointsTwo: this.props.pointsTwo,
            whichPlayer: this.props.whichPlayer
        })
    }

    render() {
        return (
            <Players>
                <Label className={this.props.greenTextOne ? 'greenText' : ''}>
                    [X]  {this.state.playerOne} - {this.state.pointsOne} PKT
                </Label>
                <Label className={this.props.greenTextTwo ? 'greenText' : ''}>
                    [O]  {this.state.playerTwo} - {this.state.pointsTwo} PKT
                </Label>
            </Players>
        )
    }
}

export default Users