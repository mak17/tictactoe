import React, { Component } from 'react';
import { Container, SubmitButton } from '../Theme/Theme.js'
import Result from './Result.js'
import Popup from './Popup'
import BorderGame from './BorderGame.js'

class Users extends Component {
    state = {
        playerOne: '',
        playerTwo: '',
        pointsPlayerOne: 0,
        pointsPlayerTwo: 0,
        char: 'X',
        whichPlayer: 0,
        border: [],
        showPopup: false,
        winner: '',
        greenTextOne: false,
        greenTextTwo: false,
        counter: 0,
        showClearButton: false,
        endGame: false
    }


    componentDidMount = () => {
        let myBorder=[[{ box: '1', col: '1', value: '', id: "id1"}, 
                       { box: '1', col: '2', value: '', id: "id2"},
                       { box: '1', col: '3', value: '', id: "id3"}],
                      [{ box: '2', col: '1', value: '', id: "id4"},
                       { box: '2', col: '2', value: '', id: "id5"}, 
                       { box: '2', col: '3', value: '', id: "id6"}],
                      [{ box: '3', col: '1', value: '', id: "id7"}, 
                       { box: '3', col: '2', value: '', id: "id8"}, 
                       { box: '3', col: '3', value: '', id: "id9"}]
                    ]

        this.setState({
            playerOne: '',
            playerTwo: '',
            pointsPlayerOne: 0,
            pointsPlayerTwo: 0,
            char: 'X',
            whichPlayer: 1,
            border: myBorder,
            showPopup: false,
            winner: '',
            greenTextOne: true,
            greenTextTwo: false,
            counter: 0,
            showClearButton: false,
            endGame: false
        })
    }

    togglePopup = () => {
        this.setState({showPopup: !this.state.showPopup})
    }

    check = () => {
        let char = '';
        let endGame = false;
        let borderToCheck = this.state.border;

        if( (borderToCheck[0][0].value === "X" && borderToCheck[0][1].value === "X" && borderToCheck[0][2].value === "X") ||
        (borderToCheck[0][0].value === "O" && borderToCheck[0][1].value === "O" && borderToCheck[0][2].value === "O") ) {
            char = borderToCheck[0][0].value;
            endGame = true;
        }

        if( (borderToCheck[1][0].value === "X" && borderToCheck[1][1].value === "X" && borderToCheck[1][2].value === "X") ||
        (borderToCheck[1][0].value === "O" && borderToCheck[1][1].value === "O" && borderToCheck[1][2].value === "O") ) {
            char = borderToCheck[1][0].value;
            endGame = true;
        }

        if( (borderToCheck[2][0].value === "X" && borderToCheck[2][1].value === "X" && borderToCheck[2][2].value === "X") ||
        (borderToCheck[2][0].value === "O" && borderToCheck[2][1].value === "O" && borderToCheck[2][2].value === "O") ) {
            char = borderToCheck[2][0].value;
            endGame = true;
        }

        if( (borderToCheck[0][0].value === "X" && borderToCheck[1][0].value === "X" && borderToCheck[2][0].value === "X") ||
        (borderToCheck[0][0].value === "O" && borderToCheck[1][0].value === "O" && borderToCheck[2][0].value === "O") ) {
            char = borderToCheck[0][0].value;
            endGame = true;
        }

        if( (borderToCheck[0][1].value === "X" && borderToCheck[1][1].value === "X" && borderToCheck[2][1].value === "X") ||
        (borderToCheck[0][1].value === "O" && borderToCheck[1][1].value === "O" && borderToCheck[2][1].value === "O") ) {
            char = borderToCheck[0][1].value;
            endGame = true;
        }

        if( (borderToCheck[0][2].value === "X" && borderToCheck[1][2].value === "X" && borderToCheck[2][2].value === "X") ||
        (borderToCheck[0][2].value === "O" && borderToCheck[1][2].value === "O" && borderToCheck[2][2].value === "O") ) {
            char = borderToCheck[0][2].value;
            endGame = true;
        }

        if( (borderToCheck[0][0].value === "X" && borderToCheck[1][1].value === "X" && borderToCheck[2][2].value === "X") ||
        (borderToCheck[0][0].value === "O" && borderToCheck[1][1].value === "O" && borderToCheck[2][2].value === "O") ) {
            char = borderToCheck[0][0].value;
            endGame = true;
        }

        if( (borderToCheck[2][0].value === "X" && borderToCheck[1][1].value === "X" && borderToCheck[0][2].value === "X") ||
        (borderToCheck[2][0].value === "O" && borderToCheck[1][1].value === "O" && borderToCheck[0][2].value === "O") ) {
            char = borderToCheck[2][0].value;
            endGame = true;
        }
        
        if(endGame) {
            if(char === "X") {
                this.setState({
                    pointsPlayerOne: this.state.pointsPlayerOne + 1 ,
                    winner: 'Player 1'
                })
            }
            if(char === "O") {
                this.setState({
                    pointsPlayerTwo: this.state.pointsPlayerTwo + 1,
                    winner: 'Player 2'
                })
            }

            this.setState({counter: 0})
            setTimeout(() => { this.togglePopup()},400)
        }
    }

    playAgain = () => {
        this.clearBoard()
        this.togglePopup()
    }

    clearBoardAfterNoFreeFields = () => {
        this.clearBoard()
    }

    clearBoard = () => {
        let myBorder = this.state.border;
        for ( let i=0; i< myBorder.length; i++) {
            for ( let j=0; j< myBorder[i].length; j++) {
                myBorder[i][j].value = "" 
            }
        }
        this.setState({
            border: myBorder,
            counter: 0,
            endGame: false,
            showClearButton: false})
    }

    changeChar = (id) => {
        let myBorder = this.state.border;
        for ( let i=0; i< myBorder.length; i++) {
            for ( let j=0; j< myBorder[i].length; j++) {
                if((myBorder[i][j].id === id)) {
                    if(this.state.whichPlayer === 1) {
                        myBorder[i][j].value = "X"
                        this.setState({
                            whichPlayer: 2,
                            greenTextOne: false,
                            greenTextTwo: true
                        })
                    } else {
                        myBorder[i][j].value = "O"
                        this.setState({
                            whichPlayer: 1,
                            greenTextOne: true,
                            greenTextTwo: false
                        })
                    }
                    this.setState({counter: this.state.counter+1})
                    this.check()
                    this.moreFreeFields()
                    
                }
            }
        }
        this.setState({border: myBorder})
    }

    moreFreeFields = () => {
       
        if(this.state.counter > 7) {
            this.setState({counter: 0,
            showClearButton: !this.state.showPopup})
        }
    }
    render() {
        return (
            <Container>
                {this.state.showPopup && <Popup winner={this.state.winner} playAgain={this.playAgain}/>}
                {
                    !this.state.showPopup && 
                    <div>
                        <Result playerOne={this.state.playerOne}
                            playerTwo={this.state.playerTwo}
                            pointsOne={this.state.pointsPlayerOne}
                            pointsTwo={this.state.pointsPlayerTwo}
                            whichPlayer={this.state.whichPlayer}
                            greenTextOne={this.state.greenTextOne}
                            greenTextTwo={this.state.greenTextTwo}/>
                            <BorderGame border={this.state.border}
                                click={this.changeChar}
                            >
                            </BorderGame>
                            {/*this.state.border.map(row => 
                                <Row>{row.map(box => 
                                    <Box id={box.id} 
                                        onClick={() => this.changeChar(box.id)}>
                                        <p className="centerText">{box.value}</p>
                                    </Box>)}
                                </Row>)*/}
                            
                    </div>
                }
                {this.state.showClearButton && 
                    <SubmitButton onClick={this.clearBoardAfterNoFreeFields}>No free places. Clear the board</SubmitButton>}
            </Container>
        )
    }
}

export default Users