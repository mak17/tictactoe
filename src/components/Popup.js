import React, { Component } from 'react';
import { Container, Label, SubmitButton } from '../Theme/Theme';

class Popup extends Component {  
    render() {  
        return (  
            <Container>
                <Label>Winner is: {this.props.winner}</Label>
                <SubmitButton onClick={this.props.playAgain}>Play again</SubmitButton>
            </Container>   
        );  
    }  
}  

export default Popup;